<?php

return [

    'user_model' => TestApp\Entities\User::class,

    'message_model' => TestApp\Entities\Messenger\Message::class,

    'participant_model' => TestApp\Entities\Messenger\Participant::class,

    'thread_model' => TestApp\Entities\Messenger\Thread::class,

    /**
     * Define custom database table names.
     */

    'messages_table' => null,

    'participants_table' => null,

    'threads_table' => null,
];
