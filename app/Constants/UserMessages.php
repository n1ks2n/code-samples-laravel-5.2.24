<?php

namespace TestApp\Constants;

class UserMessages
{
    const OFFER_ACCEPTED = 'Your offer have been accepted.';

    const OFFER_DECLINED = 'Your offer have been declined.';
}