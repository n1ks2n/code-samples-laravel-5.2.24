<?php

namespace TestApp\Constants;

use TestApp\Exceptions\OfferEntityException;

class OfferStatuses
{
    const OPEN = 0;

    const ACCEPTED = 1;

    const DECLINED = 2;

    private static $STATUS_TEXTS = [
        self::OPEN => 'Open',
        self::ACCEPTED => 'Accepted',
        self::DECLINED => 'Declined',
    ];

    public static function getStatusText(int $status) : string
    {
        if (!isset(self::$STATUS_TEXTS[$status])) {
            throw new OfferEntityException(
                ErrorExceptions::OFFER_STATUS_ERROR_MESSAGE,
                ErrorExceptions::OFFER_STATUS_ERROR_CODE
            );
        }

        return self::$STATUS_TEXTS[$status];
    }
}