<?php

namespace TestApp\Constants;

use TestApp\Exceptions\ClassifiedEntityException;

class ClassifiedStatuses
{
    const OPEN = 0;

    const CLOSED = 1;

    static private $STATUS_TEXTS = [
        self::OPEN => 'Open',
        self::CLOSED => 'Closed'
    ];

    public static function getStatusText(int $status) : string
    {
        if (!isset(self::$STATUS_TEXTS[$status])) {
            throw new ClassifiedEntityException(
                ErrorExceptions::CLASSIFIED_STATUS_ERROR_MESSAGE,
                ErrorExceptions::CLASSIFIED_STATUS_ERROR_CODE
            );
        }

        return self::$STATUS_TEXTS[$status];
    }
}