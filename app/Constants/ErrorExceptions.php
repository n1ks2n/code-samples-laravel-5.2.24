<?php

namespace TestApp\Constants;

class ErrorExceptions
{
    const USER_DATA_NOT_FOUND_OR_NOT_AUTHENTICATED_MESSAGE =
        'Пользователь не авторизован, либо в базе отсутствуют его данные';

    const USER_DATA_NOT_FOUND_OR_NOT_AUTHENTICATED_CODE = -1;

    const CLASSIFIED_STATUS_ERROR_MESSAGE =
        'Запись статуса объявления в базе не корректна';

    const CLASSIFIED_STATUS_ERROR_CODE = -2;

    const OFFER_STATUS_ERROR_MESSAGE =
        'Запись статуса объявления в базе не корректна';

    const OFFER_STATUS_ERROR_CODE = -3;
}