<?php

namespace TestApp\Services;

use Carbon\Carbon;
use TestApp\Entities\Classified;
use TestApp\Entities\Messenger\Message;
use TestApp\Entities\Messenger\Participant;
use TestApp\Entities\Messenger\Thread;
use TestApp\Entities\Offer;
use TestApp\Entities\User;
use TestApp\Repositories\Messenger\ThreadRepository;

class MessengerService
{
    /**
     * @var ThreadRepository
     */
    private $threadRepository;

    /**
     * @var Thread
     */
    private $threadEntity;

    /**
     * @var Message
     */
    private $messageEntity;

    /**
     * @var Participant
     */
    private $participantEntity;

    /**
     * @var User
     */
    private $user;

    /**
     * MessengerService constructor.
     * @param ThreadRepository $threadRepository
     * @param UserService $userService
     * @param Thread $threadEntity
     * @param Message $messageEntity
     * @param Participant $participantEntity
     */
    public function __construct(
        ThreadRepository $threadRepository,
        UserService $userService,
        Thread $threadEntity,
        Message $messageEntity,
        Participant $participantEntity
    ) {
        $this->threadRepository = $threadRepository;
        $this->threadEntity = $threadEntity;
        $this->messageEntity = $messageEntity;
        $this->participantEntity = $participantEntity;
        $this->user = $userService->getUser();
    }

    /**
     * Метод создает новое сообщение с заданной темой и массивом участников
     *
     * @param Classified $classified
     * @param Offer $offer
     * @param string $message сообщение
     * @param array $participants массив id участников
     * @return Thread
     */
    public function newMessage(
        Classified $classified,
        Offer $offer,
        string $message,
        array $participants = null
    ) : Thread
    {
        $threadName = $this->createThreadName($classified, $offer);
        // Thread
        /**
         * @var Thread $thread
         */
        $thread = $this->threadEntity->create(
            [
                'subject' => $threadName,
                'classified_id' => $classified->id,
                'offer_id' => $offer->id
            ]
        );

        // Message
        $this->messageEntity->create(
            [
                'thread_id' => $thread->id,
                'user_id' => $this->user->id,
                'body' => $message,
            ]
        );

        // Sender
        $this->participantEntity->create(
            [
                'thread_id' => $thread->id,
                'user_id' => $this->user->id,
                'last_read' => new Carbon,
            ]
        );

        if (!empty($participants)) {
            $thread->addParticipants($participants);
        }

        return $thread;
    }

    /**
     * Метод добавляет новое сообщение в тему
     *
     * @param int $threadId айдишник темы
     * @param string $message текст сообщения
     * @param array $participants массив учатсников для добавления(опционально)
     *
     * @return Thread
     */
    public function addMessageToThread(int $threadId, string $message, array $participants = []) : Thread
    {
        $thread = $this->threadRepository->find($threadId);
        $thread->activateAllParticipants();

        // Message
        $this->messageEntity->create(
            [
                'thread_id' => $thread->id,
                'user_id' => $this->user->id,
                'body' => $message,
            ]
        );

        $participant = $this->participantEntity->firstOrCreate([
            'thread_id' => $thread->id,
            'user_id' => $this->user->id,
        ]);

        $participant->last_read = new Carbon;
        $participant->save();

        // Recipients
        if (!empty($participants)) {
            $thread->addParticipants($participants);
        }

        return $thread;
    }

    /**
     * Метод находит беседу по $id и помечает ее прочитанной текущим авторизованнным пользователем
     *
     * @param int $threadId
     * @return Thread
     */
    public function getThreadById(int $threadId) : Thread
    {
        $thread = $this->threadRepository->find($threadId);

        $thread->markAsRead($this->user->id);

        return $thread;
    }

    /**
     * Метод находит все беседы пользователя
     *
     * @param User $user
     * @return \Illuminate\Database\Eloquent\Collection|null|\TestApp\Entities\Messenger\Thread[]
     */
    public function getThreadsByUser(User $user)
    {
        return $this->threadRepository->findForUser($user);
    }

    /**
     * Метод находит все беседы связанные с конкретным объявлением
     *
     * @param Classified $classified
     * @return \Illuminate\Database\Eloquent\Collection|\TestApp\Entities\Messenger\Thread[]|static[]
     */
    public function getThreadsByClassified(Classified $classified)
    {
        return $this->threadRepository->findByClassified($classified);
    }

    /**
     * Метод находит беседу по объявлению и офферу с которыми она связана и помечает ее прочитанной
     * текущим авторизованным пользователем
     *
     * @param Classified $classified
     * @param Offer $offer
     * @return null|Thread
     */
    public function getThreadByClassifiedAndOffer(Classified $classified, Offer $offer)
    {
        $thread = $this->threadRepository->findByClassifiedAndOffer($classified, $offer);

        $thread->markAsRead($this->user->id);

        return $thread;
    }

    /**
     * Метод находит все беседы связанные с залогиненым пользователем
     *
     * @return \Illuminate\Database\Eloquent\Collection|null|\TestApp\Entities\Messenger\Thread[]
     */
    public function getThreadsForLoggedUser()
    {
        $threads = $this->threadRepository->findForUser($this->user);

        return $threads;
    }

    /**
     * Метод создает тему беседы
     *
     * @param Classified $classified
     * @param Offer $offer
     * @return string
     */
    private function createThreadName(Classified $classified, Offer $offer)
    {
        return 'Classified #' . $classified->id . ' : Offer #' . $offer->id;
    }
}