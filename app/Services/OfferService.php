<?php

namespace TestApp\Services;

use Illuminate\Http\Request;
use TestApp\Constants\ClassifiedStatuses;
use TestApp\Constants\OfferStatuses;
use TestApp\Constants\UserMessages;
use TestApp\Entities\Classified;
use TestApp\Entities\Offer;
use TestApp\Entities\User;

class OfferService
{
    /**
     * @var Offer
     */
    private $entity;
    /**
     * @var User
     */
    private $user;
    /**
     * @var MessengerService
     */
    private $messengerService;

    public function __construct(Offer $entity, UserService $userService, MessengerService $messengerService)
    {
        $this->entity = $entity;
        $this->user = $userService->getUser();
        $this->messengerService = $messengerService;
    }

    /**
     * Метод создает новый оффер для объявления и начинает диалог между пользователями
     *
     * @param Classified $classified
     * @param Request $request
     * @return Offer
     */
    public function create(Classified $classified, Request $request) : Offer
    {
        $offer = $this->entity->newInstance();
        $offer->name = $request->input('name');
        $offer->user_id = $this->user->id;
        $offer->classified_id = $classified->id;
        $offer->content = $request->input('content');
        $offer->price = $request->input('price');
        $offer->save();

        if ($request->hasFile('image')) {
            $offer->addMedia($request->file('image'))->toCollection('images');
        }

        $this->messengerService->newMessage($classified, $offer, $offer->content, [$classified->user->id]);

        return $offer;
    }

    /**
     * Метод принимает оффер по конкретному предложению и отклоняет другие открытые офферы по этому предложению
     * и отсылает сообщение в беседу о принятии оффера
     *
     * @param Classified $classified
     * @param Offer $offer
     */
    public function accept(Classified $classified, Offer $offer)
    {
        $offer->status = OfferStatuses::ACCEPTED;
        $offer->save();

        foreach ($classified->offers as $linkedOffer) {
            if ($linkedOffer->id != $offer->id) {
                $this->decline($classified, $linkedOffer);
            }
        }

        $classified->status = ClassifiedStatuses::CLOSED;
        $classified->save();

        $thread = $this->messengerService->getThreadByClassifiedAndOffer($classified, $offer);
        $this->messengerService->addMessageToThread(
            $thread->id, UserMessages::OFFER_ACCEPTED
        );
    }

    /**
     * Метод отклоняет оффер по конкретному предложению
     * и отсылает в беседу сообщение о том, что оффер был отклонен
     *
     * @param Classified $classified
     * @param Offer $offer
     */
    public function decline(Classified $classified, Offer $offer)
    {
        $offer->status = OfferStatuses::DECLINED;
        $offer->save();

        $thread = $this->messengerService->getThreadByClassifiedAndOffer($classified, $offer);
        $this->messengerService->addMessageToThread(
            $thread->id, UserMessages::OFFER_DECLINED
        );
    }
}