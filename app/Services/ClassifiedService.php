<?php

namespace TestApp\Services;

use Illuminate\Http\Request;
use TestApp\Constants\OfferStatuses;
use TestApp\Entities\Classified;

class ClassifiedService
{
    private $entity;

    private $user;

    public function __construct(Classified $entity, UserService $service)
    {
        $this->entity = $entity;
        $this->user = $service->getUser();
    }

    public function create(Request $request) : Classified
    {
        $classified = $this->entity->newInstance();
        $classified->user_id = $this->user->id;
        $classified->name = $request->input('name');
        $classified->content = $request->input('content');
        $classified->price = $request->input('price');
        $classified->save();

        if ($request->hasFile('image')) {
            $classified->addMedia($request->file('image'))->toCollection('images');
        }

        return $classified;
    }

    public function canCreateOffer(Classified $classified) : bool
    {
        return
            $this->user->id !== $classified->user_id
            &&
            !$classified->offers()->where('user_id', $this->user->id)
                ->where('status', OfferStatuses::OPEN)->count();
    }

    public function getUserOfferIfExists(Classified $classified)
    {
        return $this->user->offers()->where('classified_id', $classified->id)->where('status',
            OfferStatuses::OPEN)->first();
    }
}