<?php

namespace TestApp\Services;

use Auth;
use TestApp\Entities\User;

/**
 * Class UserService
 * @package TestApp\Services
 *
 * класс обертка для получения текущего залогиненного пользователя
 * сделан для удобного мока при модульном тестировании
 */
class UserService
{
    /**
     * @var null|User
     */
    private $user;

    /**
     * UserService constructor.
     */
    public function __construct()
    {
        $this->user = Auth::user();
    }

    public function getUser() : User
    {
        return $this->user;
    }
}