<?php

namespace TestApp\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Загрузка дополнительных пакетов в зависимости от среды
        switch ($this->app->environment()) {
            case 'local':
                $this->app->register('Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider');
                break;
            default:
                break;
        }
    }
}
