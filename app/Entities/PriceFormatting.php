<?php

namespace TestApp\Entities;

trait PriceFormatting
{
    protected $priceField = 'price';

    public function getFormattedPrice()
    {
        return number_format($this->{$this->priceField}, 2);
    }
}