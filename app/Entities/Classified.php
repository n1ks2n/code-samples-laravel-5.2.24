<?php

namespace TestApp\Entities;

use Carbon\Carbon;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use TestApp\Constants\ClassifiedStatuses;
use TestApp\Constants\OfferStatuses;

/**
 * TestApp\Entities\Classified
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $content
 * @property int $status
 * @property double $price
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property User $user
 * @property Offer[] $offers
 */
class Classified extends BaseEntity implements HasMedia
{
    use HasMediaTrait, PriceFormatting;

    protected $table = 'classifieds';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Eloquent\Builder|User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Eloquent\Builder|Offer[]
     */
    public function offers()
    {
        return $this->hasMany(Offer::class, 'classified_id', 'id');
    }

    /**
     * Метод возвращает текстовый статус объявления
     *
     * @return string
     * @throws \TestApp\Exceptions\ClassifiedEntityException
     */
    public function getStatusText()
    {
        return ClassifiedStatuses::getStatusText($this->status);
    }

    /**
     * Метод возвращает кол-во открытых офферов по объявлению
     *
     * @return int
     */
    public function getOpenOffersCount()
    {
        return $this->offers()->where('status', OfferStatuses::OPEN)->count();
    }
}