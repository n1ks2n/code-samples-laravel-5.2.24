<?php

namespace TestApp\Entities;

use Carbon\Carbon;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use TestApp\Constants\OfferStatuses;

/**
 * TestApp\Entities\Offer
 *
 * @property int $id
 * @property int $user_id
 * @property int $classified_id
 * @property string $name
 * @property string $content
 * @property int $status
 * @property double $price
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property User $user
 * @property Classified $classified
 */
class Offer extends BaseEntity implements HasMedia
{
    use HasMediaTrait, PriceFormatting;

    protected $table = 'offers';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Eloquent\Builder|User
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Eloquent\Builder|Classified
     */
    public function classified()
    {
        return $this->belongsTo(Classified::class);
    }

    public function getStatusText()
    {
        return OfferStatuses::getStatusText($this->status);
    }
}