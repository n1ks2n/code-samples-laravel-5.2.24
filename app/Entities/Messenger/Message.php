<?php

namespace TestApp\Entities\Messenger;

use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message as BaseMessage;

/**
 * TestApp\Entities\Messenger\Message
 *
 * @property int $id
 * @property int $thread_id
 * @property int $user_id
 * @property string $body
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @method Message findOrFail(int $id)
 */
class Message extends BaseMessage
{

}