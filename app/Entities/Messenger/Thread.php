<?php

namespace TestApp\Entities\Messenger;

use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Thread as BaseThread;
use Illuminate\Database\Eloquent\Builder;
use TestApp\Entities\Classified;
use TestApp\Entities\Offer;

/**
 * TestApp\Entities\Messenger\Thread
 *
 * @property int $id
 * @property string $subject
 * @property int $classified_id
 * @property int $offer_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @method Thread findOrFail(int $id)
 * @method Builder ofClassifiedAndOffer(Classified $classifieds, Offer $offers)
 * @method Builder ofClassified(Classified $classifieds)
 * @method Builder forUser(int $user_id)
 */
class Thread extends BaseThread
{
    protected $fillable = ['subject', 'classified_id', 'offer_id'];

    public function classified()
    {
        return $this->belongsTo(Classified::class);
    }

    public function offer()
    {
        return $this->belongsTo(Offer::class);
    }

    /**
     * @param Builder $query
     * @param Classified $classified
     * @param Offer $offer
     *
     * @return Builder
     */
    public function scopeOfClassifiedAndOffer($query, Classified $classified, Offer $offer)
    {
        return $query->where('classified_id', $classified->id)->where('offer_id', $offer->id);
    }

    /**
     * @param Builder $query
     * @param Classified $classified
     *
     * @return Builder
     */
    public function scopeOfClassified($query, Classified $classified)
    {
        return $query->where('classified_id', $classified->id);
    }
}