<?php

namespace TestApp\Entities\Messenger;

use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Participant as BaseParticipant;

/**
 * TestApp\Entities\Messenger\Participant
 *
 * @property int $id
 * @property int $thread_id
 * @property int $user_id
 * @property Carbon $last_read
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @method Participant findOrFail(int $id)
 * @method Participant firstOrCreate(array $data)
 */
class Participant extends BaseParticipant
{

}