<?php

namespace TestApp\Entities;

use Carbon\Carbon;
use Cmgmyr\Messenger\Traits\Messagable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Foundation\Auth\Access\Authorizable;
use TestApp\Constants\ClassifiedStatuses;
use TestApp\Constants\OfferStatuses;

/**
 * TestApp\Entities\User
 *
 * @property int $id
 * @property string $name
 * @property string $login
 * @property string $password
 * @property string $email
 * @property string $phone
 * @property string $remember_token
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 * @property Classified[] $classifieds
 * @property Offer[] $offers
 */
class User extends BaseEntity implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Messagable, Authenticatable, Authorizable, CanResetPassword;

    protected $table = 'users';

    protected $fillable = ['name', 'email', 'password'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Eloquent\Builder|Classified[]
     */
    public function classifieds()
    {
        return $this->hasMany(Classified::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Eloquent\Builder|Offer[]
     */
    public function offers()
    {
        return $this->hasMany(Offer::class, 'user_id', 'id');
    }

    public function getOpenClassifieds()
    {
        return $this->classifieds()->where('status', ClassifiedStatuses::OPEN)->get();
    }

    public function getOpenClassifiedsCount()
    {
        return $this->classifieds()->where('status', ClassifiedStatuses::OPEN)->count();
    }

    public function getOpenOffers()
    {
        return $this->offers()->where('status', OfferStatuses::OPEN)->get();
    }

    public function getOpenOffersCount()
    {
        return $this->offers()->where('status', OfferStatuses::OPEN)->count();
    }
}