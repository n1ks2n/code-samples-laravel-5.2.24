<?php

namespace TestApp\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use TestApp\Entities\User;

/**
 * TestApp\Repositories\UserRepository
 *
 * @property Builder|User $entity
 * 
 * @method User findById(int $id)
 * @method Collection|User[] findAll()
 */
class UserRepository extends BaseRepository
{
    public function __construct(User $entity)
    {
        parent::__construct($entity);
    }
}