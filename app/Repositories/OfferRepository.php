<?php

namespace TestApp\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use TestApp\Constants\OfferStatuses;
use TestApp\Entities\Offer;
use TestApp\Entities\User;

/**
 * TestApp\Repositories\OfferRepository
 *
 * @property Builder|Offer $entity
 * 
 * @method Offer findById(int $id)
 * @method Collection|Offer[] findAll()
 */
class OfferRepository extends BaseRepository
{
    public function __construct(Offer $entity)
    {
        parent::__construct($entity);
    }

    /**
     * Метод находит последние открытые офферы пользователя
     *
     * @param User $user
     * @param int $limit лимит поиска объявлений по умолчанию 20
     * @return array|Collection|\TestApp\Entities\Offer[]|static[]
     */
    public function findAllOpenByUser(User $user, int $limit = 20)
    {
        return $this->entity
            ->where('user_id', $user->id)
            ->where('status', OfferStatuses::OPEN)
            ->take($limit)
            ->orderBy('id', 'desc')
            ->paginate($limit);
    }

    /**
     * Метод находит все объявления по пользователю
     *
     * @param User $user
     * @return Collection|static[]
     */
    public function findAllByUser(User $user)
    {
        return $this->entity->where('user_id', $user->id)->paginate(20);
    }
}