<?php

namespace TestApp\Repositories\Messenger;

use Illuminate\Database\Eloquent\Model;

class BaseMessengerRepository
{
    /**
     * @var \Illuminate\Database\Eloquent\Builder|Model
     */
    protected $entity;

    /**
     * BaseMessengerRepository constructor.
     * @param Model $entity
     */
    public function __construct(Model $entity)
    {
        $this->entity = $entity;
    }

    /**
     * @param int $id
     * @return \Illuminate\Database\Eloquent\Collection|Model
     */
    public function find(int $id)
    {
        return $this->entity->findOrFail($id);
    }
}