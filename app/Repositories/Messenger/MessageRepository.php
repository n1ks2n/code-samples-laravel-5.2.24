<?php

namespace TestApp\Repositories\Messenger;

use Illuminate\Database\Eloquent\Builder;
use TestApp\Entities\Messenger\Message;

/**
 * TestApp\Repositories\Messenger\MessageRepository\
 *
 * @property Builder|Message $entity
 * 
 * @method Message find(int $id)
 */
class MessageRepository extends BaseMessengerRepository
{
    /**
     * MessageRepository constructor.
     * @param Message $entity
     */
    public function __construct(Message $entity)
    {
        parent::__construct($entity);
    }
}