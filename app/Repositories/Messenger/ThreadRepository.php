<?php

namespace TestApp\Repositories\Messenger;

use Illuminate\Database\Eloquent\Builder;
use TestApp\Entities\Classified;
use TestApp\Entities\Messenger\Thread;
use TestApp\Entities\Offer;
use TestApp\Entities\User;

/**
 * TestApp\Repositories\Messenger\ThreadRepository
 * 
 * @property Builder|Thread $entity
 *
 * @method Thread find(int $id)
 */
class ThreadRepository extends BaseMessengerRepository
{
    /**
     * ThreadRepository constructor.
     * @param \Illuminate\Database\Eloquent\Builder|Thread $entity
     */
    public function __construct(Thread $entity)
    {
        parent::__construct($entity);
    }

    /**
     * @param User $user
     * @return \Illuminate\Database\Eloquent\Collection|Thread[]|null
     */
    public function findForUser(User $user)
    {
        return $this->entity->forUser($user->id)->get();
    }

    /**
     * @param string $subject
     * @param User $user
     * @return \Illuminate\Database\Eloquent\Builder|Thread|null
     */
    public function findBySubjectForUser(string $subject, User $user)
    {
        return $this->entity->forUser($user->id)->where('subject', $subject)->first();
    }

    /**
     * @param string $subject
     * @return \Illuminate\Database\Eloquent\Collection|Thread[]|null
     */
    public function findBySubject(string $subject)
    {
        return $this->entity->where('subject', 'like', '%' . $subject . '%')->get();
    }

    /**
     * @param Classified $classified
     * @param Offer $offer
     * @return Thread|null
     */
    public function findByClassifiedAndOffer(Classified $classified, Offer $offer)
    {
        return $this->entity->ofClassifiedAndOffer($classified, $offer)->first();
    }

    /**
     * @param Classified $classified
     * @return \Illuminate\Database\Eloquent\Collection|static[]|Thread[]
     */
    public function findByClassified(Classified $classified)
    {
        return $this->entity->ofClassified($classified)->get();
    }
}