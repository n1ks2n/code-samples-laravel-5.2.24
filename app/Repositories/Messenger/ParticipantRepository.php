<?php

namespace TestApp\Repositories\Messenger;

use Illuminate\Database\Eloquent\Builder;
use TestApp\Entities\Messenger\Participant;

/**
 * TestApp\Repositories\Messenger\ParticipantRepository
 *
 * @property Builder|Participant $entity
 * 
 * @method Participant find(int $id)
 */
class ParticipantRepository extends BaseMessengerRepository
{
    /**
     * ParticipantRepository constructor.
     * @param Participant $entity
     */
    public function __construct(Participant $entity)
    {
        parent::__construct($entity);
    }

}