<?php

namespace TestApp\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use TestApp\Constants\ClassifiedStatuses;
use TestApp\Entities\Classified;
use TestApp\Entities\User;

/**
 * TestApp\Repositories\ClassifiedRepository
 *
 * @property Builder|Classified $entity
 * 
 * @method Classified findById(int $id)
 * @method Collection|Classified[] findAll()
 */
class ClassifiedRepository extends BaseRepository
{
    public function __construct(Classified $entity)
    {
        parent::__construct($entity);
    }

    /**
     * Метод находит последние открытые объявления
     *
     * @param int $limit лимит поиска объявлений по умолчанию 20
     * @return array|Collection|static[]|Classified[]
     */
    public function findAllOpen(int $limit = 20)
    {
        return $this->entity->where('status', ClassifiedStatuses::OPEN)->take($limit)->orderBy('id',
            'desc')->paginate($limit);
    }

    /**
     * Метод находит все объявления по пользователю
     *
     * @param User $user
     * @return Collection|static[]
     */
    public function findAllByUser(User $user)
    {
        return $this->entity->where('user_id', $user->id)->paginate(20);
    }
}