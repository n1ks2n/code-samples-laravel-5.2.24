<?php

namespace TestApp\Repositories;

use TestApp\Entities\BaseEntity;

class BaseRepository
{
    /**
     * @var BaseEntity
     */
    protected $entity;

    /**
     * BaseRepository constructor.
     * @param BaseEntity $entity
     */
    public function __construct(BaseEntity $entity)
    {
        $this->entity = $entity;
    }

    /**
     * @param int $id
     * @return BaseEntity
     */
    public function findById(int $id) : BaseEntity
    {
        return $this->entity->find($id);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]|BaseEntity[]
     */
    public function findAll()
    {
        return $this->entity->paginate(20);
    }
}