<?php

namespace TestApp\Http\Controllers;

use Illuminate\Http\Request;
use TestApp\Repositories\ClassifiedRepository;
use TestApp\Services\ClassifiedService;
use TestApp\Services\UserService;
use Validator;

class ClassifiedController extends Controller
{
    /**
     * @var ClassifiedRepository
     */
    private $repository;

    /**
     * ClassifiedController constructor.
     * @param UserService $userService
     * @param ClassifiedRepository $repository
     */
    public function __construct(UserService $userService, ClassifiedRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct($userService);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $classifieds = $this->repository->findAllByUser($this->userService->getUser());

        return $this->view('classifieds.all', ['classifieds' => $classifieds]);
    }

    /**
     * @param int $id
     * @param ClassifiedService $service
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function classified(int $id, ClassifiedService $service)
    {
        $classified = $this->repository->findById($id);
        $canCreateOffer = $service->canCreateOffer($classified);
        $offer = $service->getUserOfferIfExists($classified);

        return $this->view(
            'classifieds.index',
            ['classified' => $classified, 'canCreateOffer' => $canCreateOffer, 'offer' => $offer]
        );
    }

    /**
     * @param Request $request
     * @param ClassifiedService $service
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request, ClassifiedService $service)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'content' => 'required',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect('classifieds')
                ->withErrors($validator)
                ->withInput();
        }

        if ($classified = $service->create($request)) {
            return redirect('classifieds/' . $classified->id);
        }

        return $this->index();
    }
}