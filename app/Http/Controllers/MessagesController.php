<?php

namespace TestApp\Http\Controllers;

use Illuminate\Http\Request;
use TestApp\Repositories\ClassifiedRepository;
use TestApp\Repositories\OfferRepository;
use TestApp\Services\MessengerService;
use TestApp\Services\UserService;
use Validator;

class MessagesController extends Controller
{
    /**
     * @var MessengerService
     */
    protected $messageService;

    /**
     * MessageController constructor.
     * @param UserService $userService
     * @param MessengerService $messengerService
     */
    public function __construct(UserService $userService, MessengerService $messengerService)
    {
        $this->messageService = $messengerService;
        parent::__construct($userService);
    }

    public function index()
    {
        $threads = $this->messageService->getThreadsByUser($this->userService->getUser());

        return $this->view('messages.all', ['threads' => $threads]);
    }

    public function conversation(
        int $classifiedId,
        int $offerId,
        ClassifiedRepository $classifiedRepository,
        OfferRepository $offerRepository
    ) {
        $classified = $classifiedRepository->findById($classifiedId);
        $offer = $offerRepository->findById($offerId);
        $thread = $this->messageService->getThreadByClassifiedAndOffer($classified, $offer);

        return $this->view('messages.index', ['thread' => $thread]);
    }

    public function update(int $threadId, Request $request)
    {
        $thread = $this->messageService->getThreadById($threadId);
        
        $validator = Validator::make(
            $request->all(),
            [
                'message' => 'required|string'
            ]
        );
        
        if($validator->fails()) {
            return redirect('/messages/classified/' . $thread->classified_id . '/offer/' . $thread->offer_id)
                ->withErrors($validator)
                ->withInput();
        }

        $this->messageService->addMessageToThread($thread->id, $request->input('message'));
        
        return redirect('/messages/classified/' . $thread->classified_id . '/offer/' . $thread->offer_id);
    }
}