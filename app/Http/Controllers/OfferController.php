<?php

namespace TestApp\Http\Controllers;

use Illuminate\Http\Request;
use TestApp\Repositories\ClassifiedRepository;
use TestApp\Repositories\OfferRepository;
use TestApp\Services\OfferService;
use TestApp\Services\UserService;
use Validator;

class OfferController extends Controller
{
    /**
     * @var OfferRepository
     */
    protected $repository;

    /**
     * OfferController constructor.
     * @param UserService $userService
     * @param OfferRepository $repository
     */
    public function __construct(UserService $userService, OfferRepository $repository)
    {
        $this->repository = $repository;

        parent::__construct($userService);
    }

    /**
     * @return mixed
     */
    public function index()
    {
        $offers = $this->repository->findAllByUser($this->userService->getUser());

        return $this->view('offers.all', ['offers' => $offers]);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function offer(int $id)
    {
        $offer = $this->repository->findById($id);

        return $this->view('offers.index', ['offer' => $offer]);
    }

    /**
     * @param int $classifiedId
     * @param Request $request
     * @param OfferService $offerService
     * @param ClassifiedRepository $repository
     */
    public function create(
        int $classifiedId,
        Request $request,
        OfferService $offerService,
        ClassifiedRepository $repository
    ) {
        $validator = Validator::make(
            $request->all(),
            [
                'name' => 'required|max:255',
                'content' => 'required',
                'price' => 'required|numeric'
            ]
        );

        if ($validator->fails()) {
            return redirect('classifieds/' . $classifiedId)
                ->withErrors($validator)
                ->withInput();
        }

        $classified = $repository->findById($classifiedId);

        $offer = $offerService->create($classified, $request);

        if ($offer) {
            return redirect('offers/' . $offer->id);
        }

        return redirect('offers');
    }

    public function accept(
        int $classifiedId,
        int $offerId,
        OfferService $offerService,
        ClassifiedRepository $repository
    ) {
        $offer = $this->repository->findById($offerId);
        $classified = $repository->findById($classifiedId);

        $offerService->accept($classified, $offer);

        return redirect('classifieds/' . $classifiedId);
    }

    public function decline(
        int $classifiedId,
        int $offerId,
        OfferService $offerService,
        ClassifiedRepository $repository
    ) {
        $offer = $this->repository->findById($offerId);
        $classified = $repository->findById($classifiedId);

        $offerService->decline($classified, $offer);

        return redirect('classifieds/' . $classifiedId);
    }
}