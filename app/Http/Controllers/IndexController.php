<?php

namespace TestApp\Http\Controllers;

use TestApp\Repositories\ClassifiedRepository;

class IndexController extends Controller
{
    public function index(ClassifiedRepository $classifiedRepository)
    {
        $lastOpenClassifieds = $classifiedRepository->findAllOpen();

        return $this->view('index', ['lastOpenClassifieds' => $lastOpenClassifieds]);
    }
}