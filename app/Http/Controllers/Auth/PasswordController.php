<?php

namespace TestApp\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\ResetsPasswords;
use TestApp\Http\Controllers\Controller;
use TestApp\Services\UserService;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->middleware('guest');

        parent::__construct($userService);
    }
}
