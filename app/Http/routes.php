<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/', 'IndexController@index');

    //classifieds routes
    Route::group(['prefix' => 'classifieds'], function () {
        //all classifieds by user
        Route::get('/', 'ClassifiedController@index');
        // classified by id
        Route::get('{id}', ['as' => 'classified', 'uses' => 'ClassifiedController@classified'])->where('id', '[0-9]+');
        //create classified
        Route::post('create', ['as' => 'classified.create', 'uses' => 'ClassifiedController@create']);
    });

    //offers routes
    Route::group(['prefix' => 'offers'], function () {
        //all offers by user
        Route::get('/', 'OfferController@index');
        //offer by id
        Route::get('{id}', ['as' => 'offer', 'uses' => 'OfferController@offer'])->where('id', '[0-9]+');
        //create offer for classified
        Route::post('{classifiedId}', ['as' => 'offer.create', 'uses' => 'OfferController@create'])
            ->where('classifiedId', '[0-9]+');
        //accept offer
        Route::get('accept/{classifiedId}/{offerId}', ['as' => 'offer.accept', 'uses' => 'OfferController@accept'])
            ->where(['classifiedId' => '[0-9]+', 'offerId' => '[0-9]+']);
        //decline offer
        Route::get('decline/{classifiedId}/{offerId}', ['as' => 'offer.decline', 'uses' => 'OfferController@decline'])
            ->where(['classifiedId' => '[0-9]+', 'offerId' => '[0-9]+']);
    });

    //messages routes
    Route::group(['prefix' => 'messages'], function () {
        //all messages by user
        Route::get('/', 'MessagesController@index');
        //conversation for offer about classified
        Route::get(
            '/classified/{classifiedId}/offer/{offerId}',
            ['as' => 'conversation', 'uses' => 'MessagesController@conversation']
        )->where(['classifiedId' => '[0-9]+', 'offerId' => '[0-9]+']);
        //create new message in conversation(thread)
        Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
    });
});
