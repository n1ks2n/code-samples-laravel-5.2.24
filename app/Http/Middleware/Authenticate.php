<?php

namespace TestApp\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Http\Request;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                if (!$this->isAuthRoute($request)) {
                    return redirect()->guest('login');
                }
            }
        }

        return $next($request);
    }

    private function isAuthRoute(Request $request) : bool
    {
        return $request->is('login') || $request->is('register') || $request->is('password/reset');
    }
}
