<?php

/**
 * @var TestApp\Entities\Classified $classified
 */
?>
@extends('layouts.app')

@section('page_header')
    <h1>My Classifieds</h1>
@endsection

@section('content')
    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Content</th>
            <th>Price</th>
            <th>Link</th>
            <th>Status</th>
            <th>Open Offers</th>
        </tr>
        </thead>
        <tbody>
        @foreach($classifieds as $classified)
            <tr>
                <td>{{$classified->id}}</td>
                <td>{{$classified->name}}</td>
                <td>{{$classified->content}}</td>
                <td>{{$classified->getFormattedPrice()}}</td>
                <td><a href="{{route('classified', ['id' => $classified->id])}}">link</a></td>
                <td>{{$classified->getStatusText()}}</td>
                <td>{{$classified->getOpenOffersCount()}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <h2>Create new classified</h2>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
     @endif
        {!! Form::open(['route' => ['classified.create'], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                <!-- offer Form Input -->
        <div class="form-group">
            <label for="name" class="control-label">Name</label>
            {!! Form::input('text', 'name', null, ['class' => 'form-control', 'id' => 'name']) !!}
        </div>

        <div class="form-group">
            <label for="image" class="control-label">Image</label>
            {!! Form::file('image') !!}
        </div>

        <div class="form-group">
            <label for="content" class="control-label">Content</label>
            {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            <label for="price" class="control-label">Price</label>
            {!! Form::input('text', 'price', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Submit Form Input -->
        <div class="form-group">
            {!! Form::submit('Submit', ['class' => 'btn btn-primary form-control']) !!}
        </div>
        {!! Form::close() !!}

        {!! $classifieds->links() !!}
@endsection
