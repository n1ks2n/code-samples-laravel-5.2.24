<?php

/**
 * @var TestApp\Entities\Classified $classified
 */
?>
@extends('layouts.app')

@section('page_header')
    <h1>{{$classified->name}}</h1>
@endsection

@section('content')
    <div class="content">
        <p class="lead">Content: {{$classified->content}}</p>
        @foreach($classified->getMedia('images') as $media)
            <img class="preview" src="{{$media->getUrl()}}" alt="{{$media->name}}"/>
        @endforeach
        <p class="lead">Price: {{$classified->getFormattedPrice()}}</p>
    </div>
    @if($canCreateOffer)
        <h2>Create a new offer</h2>
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            {!! Form::open(['route' => ['offer.create', $classified->id], 'method' => 'POST']) !!}
                    <!-- offer Form Input -->
            <div class="form-group">
                <label for="name">Name</label>
                {!! Form::input('text', 'name', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                <label for="content">Content</label>
                {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                <label for="price">Price</label>
                {!! Form::input('text', 'price', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Submit Form Input -->
            <div class="form-group">
                {!! Form::submit('Submit', ['class' => 'btn btn-primary form-control']) !!}
            </div>
            {!! Form::close() !!}
        @else
            @if($offer)
                <p>
                    You have already created <a href="{{route('offer', ['id' => $offer->id])}}">offer</a> for
                    this classified
                </p>
            @endif
        @endif
        @if($classified->user_id == $user->id && $classified->offers)
            <h2>This is your classified</h2>
            <h3>Offers for this classified</h3>
            <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>User</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Status</th>
                    <th>Actions</th>
                    <th>Link</th>
                </tr>
                </thead>
                <tbody>
                @foreach($classified->offers as $offer)
                    <tr>
                        <td>{{$offer->id}}</td>
                        <td>{{$offer->user->name}}</td>
                        <td>{{$offer->name}}</td>
                        <td>{{$offer->getFormattedPrice()}}</td>
                        <td>{{$offer->getStatusText()}}</td>
                        <td>
                            @if($classified->status == \TestApp\Constants\ClassifiedStatuses::OPEN)
                                <a class="btn btn-success"
                                   href="{!! route('offer.accept', ['classifiedId' => $classified->id, 'offerId' => $offer->id]) !!}">
                                    accept
                                </a>
                                <a class="btn btn-danger"
                                   href="{!! route('offer.decline', ['classifiedId' => $classified->id, 'offerId' => $offer->id]) !!}">
                                    decline
                                </a>
                            @else
                                Classified already closed
                            @endif
                        </td>
                        <td><a href="{{route('offer', ['id' => $offer->id])}}">link</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
@endsection