<?php

/**
 * @var TestApp\Entities\Offer $offer
 */
?>
@extends('layouts.app')

@section('page_header')
    <h1>{{$offer->name}}</h1>
@endsection

@section('content')
    <p class="lead">Content: {{$offer->content}}</p>
    @foreach($offer->getMedia('images') as $media)
        <img class="preview" src="{{$media->getUrl()}}" alt="{{$media->name}}"/>
    @endforeach
    <p class="lead">Price: {{$offer->getFormattedPrice()}}</p>
    @if($user->id == $offer->classified->user_id)
        <a class="btn btn-success"
           href="{{route('offer.accept', ['classifiedId' => $offer->classified->id, 'offerId' => $offer->id])}}">
            Accept
        </a>
        <a class="btn btn-danger"
           href="{{route('offer.decline', ['classifiedId' => $offer->classified->id, 'offerId' => $offer->id])}}">
            Decline
        </a>
        <h2>Classified</h2>
        <table class="table">
            <tbody>
            <tr>
                <td>{{$offer->classified->name}}</td>
                <td>{{$offer->classified->content}}</td>
                <td>{{$offer->classified->getFormattedPrice()}}</td>
                <td><a href="{{route('classified', ['id' => $offer->classified->id])}}">link</a></td>
            </tr>
            </tbody>
        </table>
    @endif
@endsection