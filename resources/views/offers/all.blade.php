<?php

/**
 * @var TestApp\Entities\Offer $offer
 */
?>
@extends('layouts.app')

@section('page_header')
    <h1>My Offers</h1>
@endsection

@section('content')
    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Classified</th>
            <th>Classified Price</th>
            <th>Offer Name</th>
            <th>Offer Content</th>
            <th>Offer Price</th>
            <th>Offer Status</th>
            <th>Offer Link</th>
            <th>Conversation</th>
        </tr>
        </thead>
        <tbody>
        @foreach($offers as $offer)
            <tr>
                <td>{{$offer->id}}</td>
                <td>
                    <a href="{{route('classified', ['id' => $offer->classified->id])}}">{{$offer->classified->name}}</a>
                </td>
                <td>{{$offer->classified->price}}</td>
                <td>{{$offer->name}}</td>
                <td>{{$offer->content}}</td>
                <td>{{$offer->getFormattedPrice()}}</td>
                <td>{{$offer->getStatusText()}}</td>
                <td><a href="{{route('offer', ['id' => $offer->id])}}">link</a></td>
                <td>
                    <a href="{{route('conversation', ['classifiedId' => $offer->classified->id, 'offerId' => $offer->id])}}">
                        link
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {!! $offers->links() !!}
@endsection