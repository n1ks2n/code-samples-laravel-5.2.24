@extends('layouts.app')

@section('page_header')
    <h1>{{$thread->subject}}</h1>
@endsection

@section('content')
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>User</th>
            <th>Message</th>
            <th>Posted</th>
        </tr>
        </thead>
        <tbody>
        @foreach($thread->messages as $message)
            <tr>
                <td width="20%">
                    <img src="//www.gravatar.com/avatar/{!! md5($message->user->email) !!}?s=64"
                         alt="{!! $message->user->name !!}" class="img-circle">
                    {!! $message->user->name !!}
                </td>
                <td width="65%">
                    {!! $message->body !!}
                </td>
                <td width="15%">
                    <div class="text-muted">
                        <small>{!! $message->created_at->diffForHumans() !!}</small>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <h2>Add a new message</h2>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        {!! Form::open(['route' => ['messages.update', $thread->id], 'method' => 'PUT']) !!}
                <!-- Message Form Input -->
        <div class="form-group">
            {!! Form::textarea('message', null, ['class' => 'form-control']) !!}
        </div>

        <!-- Submit Form Input -->
        <div class="form-group">
            {!! Form::submit('Submit', ['class' => 'btn btn-primary form-control']) !!}
        </div>
        {!! Form::close() !!}
        </div>
        </div>
@endsection
