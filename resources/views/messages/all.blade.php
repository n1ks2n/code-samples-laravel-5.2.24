@extends('layouts.app')

@section('page_header')
    <h1>My Conversations</h1>
@endsection

@section('content')
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Subject</th>
            <th>Classified</th>
            <th>Offer</th>
            <th>Users</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
        @foreach($threads as $thread)
            <tr>
                <td>
                    <a href="{{route('conversation', ['classifiedId' => $thread->classified->id, 'offerId' => $thread->offer->id])}}">
                        {!! $thread->subject!!}
                    </a>
                </td>
                <td>
                    <a href="{{route('classified', ['id' => $thread->classified->id])}}">
                        {{$thread->classified->name}}
                    </a>
                </td>
                <td>
                    <a href="{{route('offer', ['id' => $thread->offer->id])}}">
                        {{$thread->offer->name}}
                    </a>
                </td>
                <td>
                    {{$thread->participantsString()}}
                </td>
                <td>
                    @if($thread->isUnread($user->id))
                        <a href="{{route('conversation', ['classifiedId' => $thread->classified->id, 'offerId' => $thread->offer->id])}}">
                            <span class="btn btn-info">Unread</span>
                        </a>
                    @else
                        <a href="{{route('conversation', ['classifiedId' => $thread->classified->id, 'offerId' => $thread->offer->id])}}">
                            <span class="btn btn-default">Read</span>
                        </a>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection