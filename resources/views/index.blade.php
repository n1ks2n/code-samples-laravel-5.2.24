@extends('layouts.app')

@section('page_header')
    <h2>Last 20 Open Classifieds</h2>
@endsection

@section('content')
    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Content</th>
            <th>User</th>
            <th>Price</th>
            <th>Open Offers</th>
            <th>Link</th>
        </tr>
        </thead>
        <tbody>
        @foreach($lastOpenClassifieds as $openClassified)
            <tr>
                <td>{{$openClassified->id}}</td>
                <td>{{$openClassified->name}}</td>
                <td>{{$openClassified->content}}</td>
                <td>{{$openClassified->user->name}}</td>
                <td>{{$openClassified->price}}</td>
                <td>{{$openClassified->getOpenOffersCount()}}</td>
                <td>
                    <a href="{{route('classified', ['id' => $openClassified->id])}}">
                        Link
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
@endsection
